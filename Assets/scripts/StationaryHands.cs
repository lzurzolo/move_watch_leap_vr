﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationaryHands : MonoBehaviour {

    static public bool UseStationary;

	void Start ()
    {
        UseStationary = false;

        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == "stationary")
            {
                UseStationary = true;
                break;
            }
        }
    }
	
}
