﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using UnityEngine.UI;
using Leap;
using System;


public class MovementManager : MonoBehaviour
{
    private Leap.Controller Controller;

    [System.Serializable]
    public class Movement
    {
        public string position { get; set; }
        public string hand { get; set; }
        public float angle { get; set; }
        public float time { get; set; }
        public float prep { get; set; }
        public bool mirror { get; set; }
    }

    [Header("Hand")]
    [Tooltip("Left Index Finger Point: The transform of tip of the left index finger.")]
    public Transform LeftIndexFingerPoint;
    [Tooltip("Left Index Finger Knuckle: The transform of knuckle of the left index finger.")]
    public Transform LeftIndexFingerKnuckle;
    [Tooltip("Right Index Finger Point: The transform of tip of the right index finger.")]
    public Transform RightIndexFingerPoint;
    [Tooltip("Right Index Finger Knuckle: The transform of knuckle of the right index finger.")]
    public Transform RightIndexFingerKnuckle;
    [Tooltip("Left Hand: The game object representing the left hand.")]
    public GameObject LeftHand;
    [Tooltip("Right Hand: The game object representing the right hand.")]
    public GameObject RightHand;
    public Transform LeftHandContainer;
    public Transform RightHandContainer;

    [Header("LeapProviders")]
    public Leap.Unity.LeapServiceProvider LeftProvider;
    public Leap.Unity.LeapServiceProvider RightProvider;
    public GameObject LeftDevice;
    public GameObject RightDevice;

    [Header("Ray")]
    public GameObject LeftRay;
    public GameObject RightRay;
    private Cylinder LeftRayScript;
    private Cylinder RightRayScript;
    public Transform LeftRayEndPoint;
    public Transform RightRayEndPoint;

    [Header("HUD")]
    public Text HUDMovementsLeft;
    public Text HUDCurrentMove;
    public Text HUDPrepareToMove;

    private int NumberOfMovementsLeft;

    private float TimeforPreparation;

    [Range(1, 20)]
    public int AngleThreshold;

    public enum MirrorState
    {
        NO_MIRROR,
        LEFT_MIRROR,
        RIGHT_MIRROR
    };

    private MirrorState CurrentMirrorState;

    private List<Movement> MovementList;

    private static short CurrentMovementIndex;
    private float CurrentTime;
    private float TimeBetweenRotations;

    // Set this flag to test hand movement, this will prevent the simulation from running through movements
    static public bool HandDebugMode;

    private DataWriter Writer;
    private Comm DAQComm;

    // Use this for initialization
    void Start()
    {
        Zero();
        Controller = new Controller();
        ReadConfigFileToMovementList();
        CountNumberOfMovements();
        SetHUDCurrentMove(MovementList[CurrentMovementIndex].position);
        TimeforPreparation = MovementList[CurrentMovementIndex].time - MovementList[CurrentMovementIndex].prep;
        SetHUDNumberOfMovesLeft(NumberOfMovementsLeft);
        SetInitialMirrorModeStateVars();
        GetLoggingReferences();
        GetDAQReferences();
        GetRayReferences();

        HandDebugMode = false;
    }

    // Update is called once per frame
    void Update()
    {
        // pause simulation if less than 2 hands are in frame
        if (!AreTwoHandsVisible())
            return;

        CurrentTime += Time.deltaTime;
        // only display the prepare to move prompt after a given amount of time has passed

        if (CurrentMirrorState == MirrorState.LEFT_MIRROR)
        {
            LeftHandContainer.position = RightHandContainer.position;
        }

        if (CurrentMirrorState == MirrorState.RIGHT_MIRROR)
        {
            RightHandContainer.position = LeftHandContainer.position;
        }

        if (CurrentTime >= TimeforPreparation)
        {
            HUDPrepareToMove.enabled = true;
            if (Comm.UseDAQ)
            {
                DAQComm.SendMessageToDaq();
            }
        }
        else
        {
            HUDPrepareToMove.enabled = false;
        }

        if (!HandDebugMode)
        {
            if (CurrentTime >= TimeBetweenRotations)
            {
                // get the next movement from the movement array if it exists
                if (CurrentMovementIndex < MovementList.Count)
                {
                    StepToNextMovement();
                }
                else
                {
                    // set exit state
                    GameObject.Find("ApplicationManager").GetComponent<ApplicationManager>().IsPreparingToExit = true;
                    HUDPrepareToMove.text = "Test Completed. Exiting...";
                }
                // reset the timer
                CurrentTime = 0.0f;
            }
        }
    }

    private void StepToNextMovement()
    {
        // disable the HUD prompt in preparation for the movement change
        HUDPrepareToMove.enabled = false;

        LeftRayScript.FollowOriginalHand();
        RightRayScript.FollowOriginalHand();

        Movement newMovement = GetNextMovement();
        CurrentMovementIndex++;

        // set the time between rotations to the new movement's time
        TimeBetweenRotations = newMovement.time;

        // set the angle of the rotation to the new movement's angle
        float angle = newMovement.angle;

        // set which hand the movement is for
        string whichHand = newMovement.hand;

        if (newMovement.position == "Move")
        {
            if (whichHand == "left" || whichHand == "Left")
            {
                RightRayScript.SetInvisible();
                LeftRayScript.SetVisible();
                if (newMovement.mirror)
                {
                    CurrentMirrorState = MirrorState.LEFT_MIRROR;
                    ApplyMirroring(LeftDevice);
                    RightHand.SetActive(false);
                }
            }
            else if (whichHand == "right" || whichHand == "Right")
            {
                LeftRayScript.SetInvisible();
                RightRayScript.SetVisible();
                if (newMovement.mirror)
                {
                    CurrentMirrorState = MirrorState.RIGHT_MIRROR;
                    ApplyMirroring(RightDevice);
                    LeftHand.SetActive(false);
                }
            }
            else if (whichHand == "both" || whichHand == "Both")
            {
                CurrentMirrorState = MirrorState.NO_MIRROR;
                LeftRayScript.SetVisible();
                RightRayScript.SetVisible();
            }

            if (Comm.UseDAQ)
            {
                DAQComm.SendMessageToDaq();
            }
            NumberOfMovementsLeft--;
            SetHUDNumberOfMovesLeft(NumberOfMovementsLeft);
            HUDPrepareToMove.text = "";
        }
        else
        {
            if (CurrentMirrorState == MirrorState.LEFT_MIRROR)
            {
                UndoMirroring(LeftDevice);
                RightHand.SetActive(true);
            }

            if (CurrentMirrorState == MirrorState.RIGHT_MIRROR)
            {
                UndoMirroring(RightDevice);
                LeftHand.SetActive(true);
            }

            CurrentMirrorState = MirrorState.NO_MIRROR;

            if (Comm.UseDAQ)
            {
                DAQComm.SendMessageToDaq();
            }
            SetHUDCurrentMove("Rest");
            LeftRayScript.SetVisible();
            RightRayScript.SetVisible();
            string nextHand = MovementList[CurrentMovementIndex].hand;

            bool isNextMoveMirrored = MovementList[CurrentMovementIndex].mirror;
            TimeforPreparation = MovementList[CurrentMovementIndex - 1].time - MovementList[CurrentMovementIndex - 1].prep;

            if (!isNextMoveMirrored)
                HUDPrepareToMove.text = "Prepare to move " + nextHand;
            else
            {
                if (nextHand == "Left" || nextHand == "left")
                {
                    HUDPrepareToMove.text = "Prepare to move Left (MIRROR)";
                }
                else if (nextHand == "Right" || nextHand == "right")
                {
                    HUDPrepareToMove.text = "Prepare to move Right (MIRROR)";
                }
            }
        }
        HUDPrepareToMove.enabled = false;

        if (CurrentMirrorState == MirrorState.LEFT_MIRROR)
        {
            LeftRayScript.ApplyRotation(-angle);
            RightRayScript.ApplyRotation(angle);
        }
        else if (CurrentMirrorState == MirrorState.RIGHT_MIRROR)
        {
            LeftRayScript.ApplyRotation(angle);
            RightRayScript.ApplyRotation(-angle);
        }
        else
        {
            LeftRayScript.ApplyRotation(angle);
            RightRayScript.ApplyRotation(angle);
        }

        SetHUDCurrentMove(newMovement.position);
        Writer.LogCustomData(
            newMovement.position + "," +
            whichHand +
            "," +
            LeftIndexFingerKnuckle.position.x +
            "," +
            LeftIndexFingerKnuckle.position.y +
            "," +
            LeftIndexFingerKnuckle.position.z +
            "," +
            LeftRayEndPoint.position.x +
            "," +
            LeftRayEndPoint.position.y +
            "," +
            LeftRayEndPoint.position.z +
            "," +
            RightIndexFingerKnuckle.position.x +
            "," +
            RightIndexFingerKnuckle.position.y +
            "," +
            RightIndexFingerKnuckle.position.z +
            "," +
            RightRayEndPoint.position.x +
            "," +
            RightRayEndPoint.position.y +
            "," +
            RightRayEndPoint.position.z);

    }

    private void Zero()
    {
        CurrentMovementIndex = 0;
        NumberOfMovementsLeft = 0;
    }

    private void ReadConfigFileToMovementList()
    {
        string configFile = Path.Combine(Application.dataPath, "config.json");
        string movementData = File.ReadAllText(configFile);
        MovementList = JsonConvert.DeserializeObject<List<Movement>>(movementData);
    }

    public Movement GetNextMovement()
    {
        return MovementList[CurrentMovementIndex];
    }

    private void CountNumberOfMovements()
    {
        foreach (var m in MovementList)
        {
            if (m.position == "Move")
            {
                NumberOfMovementsLeft++;
            }
        }
    }

    public void SetHUDCurrentMove(string currentMovement)
    {
        HUDCurrentMove.text = currentMovement;
    }

    public void SetHUDNumberOfMovesLeft(int movesLeft)
    {
        HUDMovementsLeft.text = movesLeft.ToString();
    }

    private void SetInitialMirrorModeStateVars()
    {
        CurrentMirrorState = MirrorState.NO_MIRROR;
    }

    private void GetLoggingReferences()
    {
        if (DataWriter.UseLogger)
        {
            Writer = GameObject.Find("Data Writer").GetComponent<DataWriter>();
        }
    }

    private void GetDAQReferences()
    {
        if (Comm.UseDAQ)
        {
            DAQComm = GameObject.Find("DAQComm").GetComponent<Comm>();
        }
    }

    private void GetRayReferences()
    {
        LeftRayScript = LeftRay.GetComponent<Cylinder>();
        RightRayScript = RightRay.GetComponent<Cylinder>();
    }

    private bool AreTwoHandsVisible()
    {
        if (LeftProvider.CurrentFrame.Hands.Count == 0 && RightProvider.CurrentFrame.Hands.Count == 0)
            return false;
        return true;
    }

    private void ApplyMirroring(GameObject device)
    {
        Vector3 mirrorScale = new Vector3(-1, -1, -1);
        device.transform.localScale = mirrorScale;
        if (device.tag == "Left")
        {
            device.transform.Rotate(Vector3.up, 180, Space.Self);
        }
        else
        {
            device.transform.Rotate(Vector3.up, -180, Space.Self);
        }
    }

    private void UndoMirroring(GameObject device)
    {
        Vector3 normalScale = new Vector3(1, 1, 1);
        device.transform.localScale = normalScale;
        if (device.tag == "Left")
        {
            device.transform.Rotate(Vector3.up, -180, Space.Self);
        }
        else
        {
            device.transform.Rotate(Vector3.up, 180, Space.Self);
        }
    }
}
