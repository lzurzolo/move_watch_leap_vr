﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugLine : MonoBehaviour {

    public Transform Point0;
    public Transform Point1;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<LineRenderer>().SetPosition(0, Point0.position);
        GetComponent<LineRenderer>().SetPosition(1, Point1.position);
    }
}
