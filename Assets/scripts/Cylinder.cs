﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class Cylinder : MonoBehaviour
{

    private Leap.Controller controller;

    public GameObject following;
    public GameObject cylinder;
    [Range(0.0f, 1.0f)]
    public float interested;

    bool visibility = true;

    private GameObject currentFollowing;

    // Use this for initialization
    void Start()
    {
        controller = new Controller();
        currentFollowing = following;
    }

    // Update is called once per frame
    void Update()
    {
        cylinder.GetComponent<Renderer>().enabled = visibility;
    }

    void LateUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, currentFollowing.transform.position, interested);
    }

    public void ApplyRotation(float degrees)
    {
        if (tag == "Left")
        {
            transform.rotation = Quaternion.Euler(90.0f, degrees, 0.0f);
        }

        if (tag == "Right")
        {
            transform.rotation = Quaternion.Euler(90.0f, -degrees, 0.0f);
        }
    }

    public void SetInvisible()
    {
        visibility = false;
    }

    public void SetVisible()
    {
        visibility = true;
    }

    public void FollowOriginalHand()
    {
        currentFollowing = following;
    }
}
