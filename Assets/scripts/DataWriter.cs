﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using System.IO;

public class DataWriter : MonoBehaviour {

    private string DataFileName;
    private string EventFileName;
    private Leap.Controller Controller;
    private float Timer;
    public StreamWriter DataStreamWriter;
    public StreamWriter EventStreamWriter;
    private long Milliseconds;

    static public bool UseLogger = true;

    // Use this for initialization
    void Start ()
    {
        if(UseLogger)
        {
            Controller = new Controller();
            Timer = 0.0f;
            DataFileName = string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
            EventFileName = string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");


            DirectoryInfo di;
            if(Application.isEditor)
            {
                di = Directory.CreateDirectory("../log/");
            }
            else
            {
                di = Directory.CreateDirectory("log/");
            }

            // check if there are any log files in the log directory from today
            string[] dataFiles = Directory.GetFiles(di.FullName, Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyy") + "*", SearchOption.TopDirectoryOnly);
            string[] eventFiles = Directory.GetFiles(di.FullName, Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyy") + "*", SearchOption.TopDirectoryOnly);

            // get the count of the log files
            int dataCount = dataFiles.Length;
            int eventCount = eventFiles.Length;

            // if there are no data log files from today use the default name
            if (dataCount == 0)
            {
                DataFileName = string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
            }
            else // otherwise append the count to the end of the file name to prevent overwriting
            {
                DataFileName = string.Format(Application.productName + "_data_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + dataCount + ").txt");

            }

            // if there are no event log files from today use the default name
            if (eventCount == 0)
            {
                EventFileName = string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + ".txt");
            }
            else // otherwise append the count to the end of the file name to prevent overwriting
            {
                EventFileName = string.Format(Application.productName + "_event_" + System.DateTime.Today.ToString("_MMddyyyy") + "(" + eventCount + ").txt");
            }

            DataStreamWriter = new StreamWriter(di.FullName + DataFileName, true);
            EventStreamWriter = new StreamWriter(di.FullName + EventFileName, true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(UseLogger)
        {
            Frame frame = Controller.Frame();
            System.TimeSpan timeSpan;

            foreach (Hand hand in frame.Hands)
            {
                if (frame.Hands.Count > 0)
                {
                    Timer += Time.deltaTime;
                    timeSpan = System.TimeSpan.FromSeconds(Timer);

                    Milliseconds = timeSpan.Minutes * 60000 + timeSpan.Seconds * 1000 + timeSpan.Milliseconds;
                    DataStreamWriter.Write(System.DateTime.Now.ToString("h:mm::ss tt "));
                    DataStreamWriter.Write(Milliseconds);

                    DataStreamWriter.Write("," + hand.PalmPosition.x);
                    DataStreamWriter.Write("," + hand.PalmPosition.y);
                    DataStreamWriter.Write("," + hand.PalmPosition.z);

                    DataStreamWriter.Write("," + hand.PalmNormal.x);
                    DataStreamWriter.Write("," + hand.PalmNormal.y);
                    DataStreamWriter.Write("," + hand.PalmNormal.z);

                    DataStreamWriter.Write("," + hand.WristPosition.x);
                    DataStreamWriter.Write("," + hand.WristPosition.y);
                    DataStreamWriter.Write("," + hand.WristPosition.z);

                    foreach (Finger finger in hand.Fingers)
                    {

                        Bone bone;
                        foreach (Bone.BoneType boneType in (Bone.BoneType[])System.Enum.GetValues(typeof(Bone.BoneType)))
                        {

                            if (boneType.ToString() != "TYPE_INVALID")
                            {
                                bone = finger.Bone(boneType);

                                DataStreamWriter.Write("," + bone.PrevJoint.x);
                                DataStreamWriter.Write("," + bone.PrevJoint.y);
                                DataStreamWriter.Write("," + bone.PrevJoint.z);
                            }
                        }

                        DataStreamWriter.Write("," + finger.TipPosition.x);
                        DataStreamWriter.Write("," + finger.TipPosition.y);
                        DataStreamWriter.Write("," + finger.TipPosition.z);
                    }

                    DataStreamWriter.Write(System.DateTime.Today.ToString("," + "MM/dd/yyyy"));
                    DataStreamWriter.Write("\n");
                }
            }
        }
    }

    private void OnDestroy()
    {
        if(UseLogger)
        {
            DataStreamWriter.Close();
            EventStreamWriter.Close();
        }
    }

    public void LogCustomData(string input)
    {
        EventStreamWriter.Write(System.DateTime.Now.ToString("h:mm::ss tt "));
        EventStreamWriter.Write(Milliseconds);
        EventStreamWriter.Write("," + input);
        EventStreamWriter.Write(System.DateTime.Today.ToString("," + "MM/dd/yyyy"));
        EventStreamWriter.Write("\n");
    }
}
